package com.oxygen.o2.recycleviewundvolly.Model;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by O2 on 5/12/2017.
 */

public class NewsFeeds {
    public String title, imageURL, description, releaseDate;
    public Double price;
    public boolean available;

    public Boolean booked = false;
    public int p_id;
    public Integer b_id;
    public static List<NewsFeeds> feedsListN = new ArrayList<NewsFeeds>();

    public NewsFeeds(int p_id, Integer b_id, String title, String imageURL, String description, Double price, int available, String releaseDate, String bookMarks) {
        this.title = title;
        this.imageURL = imageURL;
        this.description = description;
        this.price = price;

        if (available == 1) {
            this.available = true;
        } else {
            this.available = false;
        }
        this.releaseDate = releaseDate;
        if (bookMarks != "null") this.booked = true;
        else this.booked = false;
        this.b_id = b_id;
        this.p_id = p_id;

    }

    public static List<NewsFeeds> getFeedsListN() {
//        if (feedsListN.size() == 0) {//get form local cache
//
//        } else
//
       return feedsListN;
    }

    public static void addItem(NewsFeeds f) {
        feedsListN.add(f);
    }
    public static void removeItem(NewsFeeds f) {
        feedsListN.remove(f);
    }

    public int getP_id() {
        return p_id;
    }

    public Integer getB_id() {
        return b_id;
    }

    public Boolean isBookMarks() {
        return booked;
    }

    public String getTitle() {
        return title;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getDescription() {
        return description;
    }

    public Double getPrice() {
        return price;
    }

    public boolean isAvailable() {
        return available;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setBooked(boolean booked) {
        this.booked = booked;
    }

    public void savetoDb(NewsFeeds f) {
        //add to current static list und to the db

    }

//    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
//    public void CachTheList(Context context) {
//        SharedPreferences shared = context.getSharedPreferences("newsFeedData", 0);
//        SharedPreferences.Editor editor = shared.edit();
//        Gson gson = new Gson();
//        for (int i = 0; i < feedsListN.size(); i++) {
//            String json = gson.toJson(feedsListN.get(i));
//            editor.putString("MyObject"+i, json);
//        }
//editor.putInt("feedListSize",feedsListN.size());
//        editor.commit();
//    }

//    public static String getAsString(NewsFeeds obj){
//        return obj.getTitle()+"--"+obj.getImage().split("500")[1]+"--"+obj.getRelease_date()+"--"+obj.getRatebar()+"--"+obj.getOverView();
//    }
}
