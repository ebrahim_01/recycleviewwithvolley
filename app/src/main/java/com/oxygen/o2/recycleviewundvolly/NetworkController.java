package com.oxygen.o2.recycleviewundvolly;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;

/**
 * Created by O2 on 5/12/2017.
 */

public class NetworkController {
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static Context mCtx;
    private static NetworkController mInstance;




    public NetworkController(Context context) {
        this.mCtx = context;
        mRequestQueue = getRequestQueue();
        // This will Load Images from Network in Separate Thread
        mImageLoader = new ImageLoader(mRequestQueue,
                new ImageLoader.ImageCache() {
                    //Create ImageCache of max size 10MB
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<String, Bitmap>(10);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });
    }




    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }





    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }





    public static synchronized NetworkController getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new NetworkController(context);
        }
        return mInstance;
    }








    public ImageLoader getImageLoader() {
        return mImageLoader;
    }





public void removeCachedData(Context context){
    //clear the storage
   getRequestQueue().getCache().clear();
}
//  Send response whrn the ressponse is alraey cached
    public JSONArray getNewsFeedFromCachedData(String url,Context context){
        Cache cache =getRequestQueue().getCache();
        Cache.Entry entry = cache.get(url);
        JSONArray jsonArray=null;
        if(entry != null){
            //Cache data available.
            try {
                String data = new String(entry.data, "UTF-8");
                Log.d("CACHE DATA", data);
                jsonArray=new JSONArray(data);


                Toast.makeText(context, "Loading from cache...", Toast.LENGTH_LONG).show();

            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
            }
            return jsonArray;
        }
        else{
            Toast.makeText(context, "NO cached data", Toast.LENGTH_LONG).show();
        }
        return jsonArray;
    }
}
