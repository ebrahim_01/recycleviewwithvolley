package com.oxygen.o2.recycleviewundvolly;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.oxygen.o2.recycleviewundvolly.Model.NewsFeeds;

import java.util.List;

/**
 * Created by O2 on 5/12/2017.
 */

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyViewHolder> {
    private Context context;
    private List<NewsFeeds> feedsList;
    private LayoutInflater inflater;
    public MyRecyclerAdapter(Context context, List<NewsFeeds> feedsList) {
        this.context = context;
        this.feedsList=feedsList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.singleitem_recyclerview, parent, false);
        return new MyViewHolder(rootView);
    }

    public void setFeedsList(List<NewsFeeds> feedsList) {
        this.feedsList = feedsList;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NewsFeeds feeds = feedsList.get(position);
        //Pass the values of feeds object to Views
        holder.title.setText(feeds.getTitle());
        holder.discription.setText(feeds.getDescription());
       // System.out.println(feeds.getImage());
        holder.imageview.setImageUrl(feeds.getImageURL(), NetworkController.getInstance(context).getImageLoader());
        holder.price.setText(feeds.getPrice().toString());
        if(feeds.isAvailable())
            {
            holder.avalable.setText("ava");
            holder.avalable.setTextColor(Color.parseColor("#059922"));
            }
            else{
            holder.avalable.setText("Not ava");
            holder.avalable.setTextColor(Color.parseColor("#960000"));
            }
        holder.dateView.setText( feeds.getReleaseDate());
        if(feeds.isBookMarks())
            holder.sigelItem_recycle.setBackgroundColor(Color.parseColor("#fc9803"));
        else holder.sigelItem_recycle.setBackgroundColor(Color.parseColor("#727272"));
      
    }

    @Override
    public int getItemCount() {
        return feedsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        // init the components
        private TextView discription, title,price,avalable,dateView;
        private NetworkImageView imageview;

        private LinearLayout sigelItem_recycle;
        public MyViewHolder(View itemView) {
            super(itemView);
            // defain the components
            sigelItem_recycle=(LinearLayout) itemView.findViewById(R.id.sigelItem_recycle);
            title = (TextView) itemView.findViewById(R.id.title_view);
            discription = (TextView) itemView.findViewById(R.id.content_view);
            // Volley's NetworkImageView which will load Image from URL
            imageview = (NetworkImageView) itemView.findViewById(R.id.thumbnail);
            price = (TextView) itemView.findViewById(R.id.price_view);
            avalable = (TextView) itemView.findViewById(R.id.ava_view);
            dateView = (TextView) itemView.findViewById(R.id.date_view);
            sigelItem_recycle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Intent intent=new Intent(context,DetailsScrollingActivity.class);
//                    intent.putExtra("movie",NewsFeeds.getAsString(feedsList.get(getAdapterPosition())));
//                    context.startActivity(intent);
                    feedsList.get(getAdapterPosition()).setBooked(!feedsList.get(getAdapterPosition()).isBookMarks());
                    HomeActivity.adapter.notifyItemChanged(getAdapterPosition());
                     Toast.makeText(context, "Rated By User : " + feedsList.get(getAdapterPosition()).isBookMarks(), Toast.LENGTH_SHORT).show();
                }
            });

        }
    }
}
