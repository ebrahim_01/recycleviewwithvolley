package com.oxygen.o2.recycleviewundvolly;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.oxygen.o2.recycleviewundvolly.Model.NewsFeeds;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    RequestQueue queue;
    LinearLayout linearLayout;
    String url = "http://10.0.3.2/dbtest/getproducts.php";
    RecyclerView recyclerView;
    public List<NewsFeeds> feedsList = new ArrayList<NewsFeeds>();
    public static MyRecyclerAdapter adapter;
    ProgressBar progressBar;
    SwipeRefreshLayout mySwipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_layout);
       // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        linearLayout = (LinearLayout) findViewById(R.id.mainBackground);
       progressBar = (ProgressBar) findViewById(R.id.progressBar);
      //  setSupportActionBar(toolbar);
        mySwipeRefreshLayout=(SwipeRefreshLayout)findViewById(R.id.swiperefresh);

        mySwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                LoadData();
                mySwipeRefreshLayout.setRefreshing(false);
            }
        });
//Initialize RecyclerView
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        adapter = new MyRecyclerAdapter(this, NewsFeeds.getFeedsListN());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setNestedScrollingEnabled(true);
        recyclerView.setAdapter(adapter);

        //Getting Instance of Volley Request Queue
        queue = NetworkController.getInstance(this).getRequestQueue();
        //Volley's inbuilt class to make Json array request

        LoadData();

    }
    //Volley's inbuilt class to make Json array request
public void LoadData(){
    JsonArrayRequest newsReq = new JsonArrayRequest(url,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    //  queue.getCache().remove(url);
                    HandelResponse(response);
                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError volleyError) {
            // progressBar.setVisibility(ProgressBar.VISIBLE);
            String message = null;
            if (volleyError instanceof NetworkError) {
                message = "Cannot connect to Internet...Please check your connection!";
            } else if (volleyError instanceof ServerError) {
                message = "The server could not be found. Please try again after some time!!";
            } else if (volleyError instanceof AuthFailureError) {
                message = "Cannot connect to Internet...Please check your connection!";
            } else if (volleyError instanceof ParseError) {
                message = "Parsing error! Please try again after some time!!";
            } else if (volleyError instanceof NoConnectionError) {
                message = "Cannot connect to Internet...Please check your connection!";
            } else if (volleyError instanceof TimeoutError) {
                message = "Connection TimeOut! Please check your internet connection.";
            }

            Snackbar.make(linearLayout, message, Snackbar.LENGTH_LONG)
                    .setAction("Setting", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
                        }
                    }).show();

//                Toast.makeText(HomeActivity.this, "error getting the data : " + message, Toast.LENGTH_LONG).show();
            System.out.println("Error Loading data ...: " + volleyError.getMessage());

            HandelResponse(NetworkController.getInstance(HomeActivity.this).getNewsFeedFromCachedData(url, HomeActivity.this));

        }
    });
    newsReq.setRetryPolicy(new DefaultRetryPolicy(20000,1,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

    //Adding JsonArrayRequest to Request Queue

    queue.add(newsReq);
}
    private void HandelResponse(JSONArray jsonArray) {
        int b_id = 0;
        //  check if jsonarray from the cached data available
        if (jsonArray == null) {
            progressBar.setVisibility(ProgressBar.GONE);
            return;
        }
        NewsFeeds.feedsListN.clear();
        adapter.setFeedsList(NewsFeeds.feedsListN);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = null;

            try {
                obj = jsonArray.getJSONObject(i);

//                        p_id": "1",
//                        "title": "Yellow Triangle",
//                            "imageURL": "https://kredit.check24.de/konto-kredit/ratenkredit/nativeapps/imgs/08.png",
//                            "available": "1",
//                            "releaseDate": "2017-05-01",
//                            "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam\",",
//                            "price": "225.91",
//                            "b_id": "1",
//                            "f_u_id": "1",
//                            "f_p_id": "1",
//                            "booked": "1"
                //(int p_id,int b_id,String title, String imageURL, String description, float price, boolean available, Date releaseDate, boolean bookMarks)
                if (obj.get("b_id") == null) b_id = 0;
                NewsFeeds feeds = new NewsFeeds(obj.getInt("p_id"), b_id, obj.getString("title"), obj.getString("imageURL"), obj.getString("description"), obj.getDouble("price"), obj.getInt("available"), obj.getString("releaseDate"), String.valueOf(obj.get("booked")));

                // adding movie to movies array
                //feedsList.add(feeds);

                NewsFeeds.addItem(feeds);

                adapter.notifyItemChanged(i);


            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

        }
       progressBar.setVisibility(ProgressBar.GONE);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
//for the search bar : 1.add view to the menu. 2.here
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the home_layout/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.about_me:
                openMyProfile();
                return true;
            case R.id.action_settings:
                openSettings();
                return true;
            case R.id.sort:
                sortTheList();
                return true;
            case R.id.bookmarks:
                filterBooking();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
        //return super.onOptionsItemSelected(item);
    }

    private void openSettings() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    void openMyProfile() {
        Intent intent = new Intent(this, MyProfile.class);
        startActivity(intent);

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }
// here 3.
    @Override
    public boolean onQueryTextChange(String query) {
      //  Toast.makeText(HomeActivity.this, query, Toast.LENGTH_LONG).show();
        if(query.length()==0)
        {adapter.setFeedsList(NewsFeeds.feedsListN);
        adapter.notifyDataSetChanged();}
        else {
            final List<NewsFeeds> filteredModelList = searchfilter(NewsFeeds.feedsListN, query);
            adapter.setFeedsList(filteredModelList);
            adapter.notifyDataSetChanged();
        }
        return true;
    }
    //normal search 4.
    private static List<NewsFeeds> searchfilter(List<NewsFeeds> models, String query) {
        final String lowerCaseQuery = query.toLowerCase();

        final List<NewsFeeds> filteredModelList = new ArrayList<NewsFeeds>();
        for (int i=0;i<models.size();i++) {
            final String text = models.get(i).getTitle().toLowerCase();

            if (text.contains(lowerCaseQuery)) {
               //adapter.notifyItemRemoved(i);
                 filteredModelList.add(models.get(i));
            }
        }
        return filteredModelList;
    }
    //filter for booked products
    private static void filterBooking() {

        final List<NewsFeeds> filteredModelList = new ArrayList<NewsFeeds>();
        for (int i=0;i<NewsFeeds.feedsListN.size();i++) {

            if (NewsFeeds.feedsListN.get(i).isBookMarks()) {
                //adapter.notifyItemRemoved(i);
                filteredModelList.add(NewsFeeds.feedsListN.get(i));
            }
        }
        adapter.setFeedsList(filteredModelList);
        adapter.notifyDataSetChanged();
    }
    //Sort the prices
public void sortTheList(){
    Collections.sort(NewsFeeds.feedsListN, new Comparator<NewsFeeds>() {
        @Override
        public int compare(NewsFeeds lhs, NewsFeeds rhs) {
           if(lhs.getPrice()>rhs.getPrice())
            return -1;
            else if (lhs.getPrice()<rhs.getPrice())
                return 1;
            else
                return 0;
        }
    });
    adapter.notifyDataSetChanged();
}
}
